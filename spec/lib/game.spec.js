const createGame = require('../../src/game');

describe('User can play game', () => {
  it('and create teams with monpoke, and choose attackers', () => {
    const createCommands = 'CREATE Rocket Meekachu 3 1\nCREATE Rocket Rastly 5 6\nCREATE Green Smorelax 2 1\n';
    const round1Commands = 'ICHOOSEYOU Meekachu\nICHOOSEYOU Smorelax\nATTACK\nATTACK\n';
    const round2Commands = 'ICHOOSEYOU Rastly\nATTACK\nATTACK\n';

    const commands = `${createCommands}${round1Commands}${round2Commands}`;
    const gameOutput = createGame(commands);

    const createResults = 'Meekachu has been assigned to team Rocket!\nRastly has been assigned to team Rocket!\nSmorelax has been assigned to team Green!\n';
    const round1Results = 'Meekachu has entered the battle!\nSmorelax has entered the battle!\nMeekachu attacked Smorelax for 1 damage!\nSmorelax attacked Meekachu for 1 damage!\n';
    const round2Results = 'Rastly has entered the battle!\nSmorelax attacked Rastly for 1 damage!\nRastly attacked Smorelax for 6 damage!\n';
    const battleResults = 'Smorelax has been defeated!\nRocket is the winner!';

    const expectedOutput = `${createResults}${round1Results}${round2Results}${battleResults}`;
    expect(gameOutput).toBe(expectedOutput);
  });

  it('will error out with malformed command verb', () => {
    const commands = 'OHNO Rocket Meekachu 3 1';
    const gameOutput = createGame(commands);

    expect(gameOutput).toBe('process.exit(1)');
  });
});
