const error = require('./error');

// eslint-disable-next-line no-unused-vars
module.exports = ([commandName, monpokeName], gameData) => {
  if (gameData.teams.length < 2) {
    return error();
  }

  const currentTeamMonpokeList = gameData.teams[gameData.turnTeamId].monpokeList;
  const monpokeId = currentTeamMonpokeList.findIndex(monpoke => monpoke.name === monpokeName);
  if (monpokeId === -1 || currentTeamMonpokeList[monpokeId].hp < 1) {
    return error();
  }

  const { turnTeamId } = gameData;

  /* eslint-disable no-param-reassign */
  gameData.teams[turnTeamId].monpokeList = currentTeamMonpokeList.map((monpoke, id) => {
    if (monpokeId === id) {
      return { ...monpoke, inBattle: true };
    }
    return { ...monpoke, inBattle: false };
  });

  gameData.turnTeamId = turnTeamId === 0 ? 1 : 0;
  /* eslint-enable no-param-reassign */

  return {
    gameData,
    result: `${monpokeName} has entered the battle!`,
  };
};
