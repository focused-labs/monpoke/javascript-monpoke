module.exports = () => (process.env.NODE_ENV === 'testing' ? 'process.exit(1)' : process.exit(1));
