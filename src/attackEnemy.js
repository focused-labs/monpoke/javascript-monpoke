const error = require('./error');

module.exports = gameData => {
  if (gameData.teams.length < 2) {
    return error();
  }

  const currentTeamId = gameData.turnTeamId;
  const currentTeamList = gameData.teams[currentTeamId].monpokeList;
  const currentMonpoke = currentTeamList[currentTeamList.findIndex(monpoke => monpoke.inBattle)];

  const enemyTeamId = currentTeamId === 0 ? 1 : 0;
  const enemyMonpokeList = gameData.teams[enemyTeamId].monpokeList;
  const enemyMonpokeId = enemyMonpokeList.findIndex(monpoke => monpoke.inBattle);
  const enemyMonpoke = gameData.teams[enemyTeamId].monpokeList[enemyMonpokeId];

  const currentMonpokeDefeated = currentMonpoke.hp <= 0;
  const enemyMonpokeDefeated = enemyMonpoke.hp <= 0;
  if (currentMonpokeDefeated || enemyMonpokeDefeated) {
    return error();
  }

  /* eslint-disable no-param-reassign */
  gameData.teams[enemyTeamId].monpokeList[enemyMonpokeId].hp -= currentMonpoke.ap;
  gameData.turnTeamId = gameData.turnTeamId === 0 ? 1 : 0;
  /* eslint-enable no-param-reassign */

  const result = `${currentMonpoke.name} attacked ${enemyMonpoke.name} for ${currentMonpoke.ap} damage!`;

  const enemyIsDefeated = enemyMonpokeList.every(monpoke => monpoke.hp <= 0);
  if (enemyIsDefeated) {
    const defeatedMonpokeResult = `${enemyMonpoke.name} has been defeated!`;
    const winningTeamResult = `${gameData.teams[currentTeamId].name} is the winner!`;
    return {
      gameData,
      result: `${result}\n${defeatedMonpokeResult}\n${winningTeamResult}`,
    };
  }
  return {
    gameData,
    result,
  };
};
